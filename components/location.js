class Location extends HTMLElement {
	constructor() {
		super();

		this.setAttribute('id', 'exile-location');

		const name = document.createElement('h3');
		name.setAttribute('class', 'name');

		const description = document.createElement('p');
		description.setAttribute('class', 'description');

		const container = document.createElement('div');
		container.setAttribute('class', 'list-item');
		container.innerHTML +=[
			name.outerHTML,
			description.outerHTML,
		].join('');

		const template = document.createElement('template');
		template.setAttribute('id', 'location-template');
		template.innerHTML = container.outerHTML;

		const shadow = this.attachShadow({ mode: 'open' });
		shadow.appendChild(template);
	}
}

customElements.define('exile-location', Location);
