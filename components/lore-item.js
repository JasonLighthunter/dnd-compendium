class LoreItem extends HTMLElement {
	constructor() {
		super();

		this.setAttribute('id', 'exile-lore-item');

		const name = document.createElement('h3');
		name.setAttribute('class', 'name');

		const content = document.createElement('p');
		content.setAttribute('class', 'content');

		const container = document.createElement('div');
		container.setAttribute('class', 'list-item');
		container.innerHTML +=[
			name.outerHTML,
			content.outerHTML,
		].join('');

		const template = document.createElement('template');
		template.setAttribute('id', 'lore-item-template');
		template.innerHTML = container.outerHTML;

		const shadow = this.attachShadow({ mode: 'open' });
		shadow.appendChild(template);
	}
}

customElements.define('exile-lore-item', LoreItem);
