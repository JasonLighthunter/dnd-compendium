class Character extends HTMLElement {
	constructor() {
		super();

		this.setAttribute('id', 'exile-character');

		const name = document.createElement('h3');
		name.setAttribute('class', 'name');

		const status = document.createElement('h4');
		status.setAttribute('class', 'status');

		const description = document.createElement('p');
		description.setAttribute('class', 'description');

		const container = document.createElement('div');
		container.setAttribute('class', 'list-item');
		container.innerHTML +=[
			name.outerHTML,
			status.outerHTML,
			description.outerHTML,
		].join('');

		const template = document.createElement('template');
		template.setAttribute('id', 'character-template');
		template.innerHTML = container.outerHTML;
		// template.appendChild(container)
		// template.appendChild(description);
		// template.appendChild(status);
		// template.appendChild(name);

		const shadow = this.attachShadow({ mode: 'open' });
		shadow.appendChild(template);
	}
}

customElements.define('exile-character', Character);
