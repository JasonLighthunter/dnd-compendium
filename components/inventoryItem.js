class InventoryItem extends HTMLElement {
	constructor() {
		super();

		this.setAttribute('id', 'exile-inventory-item');

		const name = document.createElement('span');
		name.setAttribute('class', 'name');

		const count = document.createElement('span');
		count.setAttribute('class', 'count');

		const container = document.createElement('div');
		container.setAttribute('class', 'list-item');
		container.innerHTML +=[
			count.outerHTML,
      ' x  : ',
      name.outerHTML,
		].join('');

		const template = document.createElement('template');
		template.setAttribute('id', 'inventory-item-template');
		template.innerHTML = container.outerHTML;

		const shadow = this.attachShadow({ mode: 'open' });
		shadow.appendChild(template);
	}
}

customElements.define('exile-inventory-item', InventoryItem);
