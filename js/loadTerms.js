const terms = [
	{
		"name": `Karui (Orc)`,
		"description" : `
			An old Orc tribe bearing this name and also the name of the language these Orcs spoke.
			They erased the southernmost part of the Eternal Empire but retreated back home shortly after settling.
		`
	},
	{
		"name": `Eternal (Elven)`,
		"description" : `
			The most recent recorded civilization that was established in Wreaclast.
			A great deal of Oriath's culture stems from the Eternal Empire.
		`
	},
	{
		"name": `Azmeri (Dwarven)`,
		"description" : `
			Not much is known about the Azmeri except the Eternal Empire wiped references to them form the annals of history.
		`
	},
	{
		"name": `Vaal (Undercommon)`,
		"description" : `
			Both the name of one of, or 'the', first civilization that left its mark on Wreaclast and the languages spoken by them.
		`
	},
	{
		"name": `Utula`,
		"description" : `
			Kobold word for "Chosen One" supposedly the Utula will bring freedom to all of koboldkind.
		`
	},
	{
		"name": `Blackguard`,
		"description" : `
			A term for the lower ranked Templar soldiers.
			They are named so because of their pitch black armor.
		`
	},
];

function laodTerms() {
	const fragment = document.getElementById('exile-term').shadowRoot.getElementById('term-template');

	terms.forEach(c => {
		const instance = document.importNode(fragment.content, true);

		instance.querySelector('.name').innerHTML = c.name;
		instance.querySelector('.description').innerHTML = c.description;

		document.getElementById('terms').appendChild(instance);
	})
}

window.onload = () => {
	laodTerms();
};
