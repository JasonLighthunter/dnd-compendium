const locations = [
	{
		"name": `Oriath`,
		"description" : `
			An island nation ruled by the Templar Order of Innocence. A religions group that is a remnant of older Wreaclastian beliefs. Started the annual exiles a few decades ago. Think New Zealand but with a religious totalitarian Government.
		`
	},
	{
		"name": `Wreaclast`,
		"description" : `
			A continent that went down in history as the most dangerous place on earth. Many civilizations have ended and some claim it consiously tries to kill you, as it it was alive. Think Australia but deadlier with magic.
		`,
	},
	{
		"name": `Lioneye's Watch`,
		"description" : `
			Southernmost outpost of the Eternal Empire. It was an military base for Marceus Lioneye's Eternal Legion. Nowadays it is home to regugees exiled from Oriath.
		`,
	},
	{
		"name": `The Eternal Empire`,
		"description" : `
			A great empire that was build by elves long ago that suddenly ceased to exist some 100 years ago.
			Spanning the southern part of the Wreaclastian continent.
		`
	},
	{
		"name": `Atzoatl`,
		"description" : `
			Ancient lost Vaal temple.
			Alva Valai is looking for it and claims it is full of gold, weapons and magical artifacts.
		`
	},
	{
		"name": `Axiom Prison`,
		"description" : `
			Eternal Empire prison Northwest of Lioneye's Watch.
			Doubled as a laboratory for Shavronne of Umbra.
		`
	},
	{
		"name": `"The lakeside laboratory"`,
		"description" : `
			Before heading to her actual destination Piety had to visit a certain laboratory.
		`
	},
	{
		"name": `Sarn`,
		"description" : `
			The destination of the expedition led by Piety according to Tony.
		`
	},
	// {
	// 	"name": `"The Black Forest"`,
	// 	"description" : `
	// 		???
	// 	`
	// },
];

function loadLocations() {
	const fragment = document.getElementById('exile-location').shadowRoot.getElementById('location-template');

	locations.forEach(c => {
		const instance = document.importNode(fragment.content, true);

		instance.querySelector('.name').innerHTML = c.name;
		instance.querySelector('.description').innerHTML = c.description;

		document.getElementById('locations').appendChild(instance);
	})
}

window.onload = () => {
	loadLocations();
};
