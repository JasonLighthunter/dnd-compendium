const handouts = [
  {
    "name": "Navali's Answer to the question: `When will the kobolds be liberated?`",
    "content": `
       Within a year.
    `
  },
  {
    "name": "Navali's Answer to the question: `When will I see Eloise?`",
    "content": `
       Within a week or so.
    `
  },
  {
    "name": "Navali's Answer to the question: `Will I die a glorious death?`",
    "content": `
       Certainly.
    `
  },
  {
    "name": "Letter found in room behind the one where the monstrocity Brutus was fought",
    "content": `
      My loyal Declan,<br>
      Give our Lady Piety your absolute support and watch her with absolute scrutiny.
      There is much to learn from the likes of the Inquisitor, the Umbra and the Soulless One.
      I would hate for Piety to feel that she need shoulder such perilous wisdom alone.<br>
      Sincerely, Dominus
    `
  },
  {
    "name": "Journal found in a Room in Axiom Prison",
    "content": `
      Still the Karui barbarians advance upon us.
      Lioneye is dead, his legion slaughtered, along with every Eternal man, woman and child from Lioneye's Watch to the foot of our Axiom.
      Should the need arise we shall retreat through Prisoners Gate, raising my barricade behind us.<br>
      Yet our salvation is at hand. Foul times demand heroic deeds, and through my arts our Lord Brutus will arise anew to defend us.
      May the dry sands quench their thirst with Karui blood when our mighty Warden delivers his judgement upon them.
    `
  },
	{
		"name": "Text spoken by unknown voice upon touching the door to the bronze trials in Axiom prison.",
		"content": `
			In the court of the Goddess, every man and woman is deemed worthy of redemption.<br>
      Though you might be shackeled by the tribulations of the past, the trials ahead offer you both freedom and glory.
      Thre future is yours, if you are bold enough to reach out for it.
		`
	},
	{
		"name": "Drawing on the pedestal at the end of the bronze Trial",
		"content": `
			A depiction of some sort of map. Axiom prison is clearly depicted with a glowing skull-like icon.<br>
      Similair icons, but less glowing, are depicted on a bluff northeast of the prison. On a location near a lake north of the prison.
      And three skulls are marked closely together even further north.
		`
	},
	{
		"name": "Letter found in camp in the forest where you met Tony",
		"content": `
			Arteri, my beautiful captain.<br><br>
			I wish it were not you, but I cannot bring myself to trust any other with this most vital of tasks.<br>
			This is the only pass between the inner and outer Empire.<br>
			No further exiles are to enter the inner Empire.<br>
			We have material enough for our work.<br>
			Ensure that the barricade remains in place, and if any exile should somehow pass through, kill them.<br>
			I will send for you when my work is complete in Sarn.
			Until we share our next night together,
			<br/>
			Piety
		`
	}
];

function loadLoreItems() {
	const fragment = document.getElementById('exile-lore-item').shadowRoot.getElementById('lore-item-template');

	handouts.forEach(h => {
		const instance = document.importNode(fragment.content, true);

		instance.querySelector('.name').innerHTML = h.name;
		instance.querySelector('.content').innerHTML = h.content;

		document.getElementById('lore-items').appendChild(instance);
	})
};

window.onload = () => {
	loadLoreItems();
};
