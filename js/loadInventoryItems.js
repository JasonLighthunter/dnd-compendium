const inventoryItems = [
	{
		"name": `Silver Coin`,
		"count" : 0
	},
	{
		"name": `"Gold Rimmed Gems (Red)"`,
		"count": 0
	},
  {
    "name": `"Gold Rimmed Gems (Blue)"`,
    "count": 0
  },
  {
    "name": `"Red Rimmed Gems (Blue)"`,
    "count": 1
  },
  {
    "name": `"Gold Rimmed Gems (Green)"`,
    "count": 0
  },
	{
		"name": `"Map Device (Disabled)"`,
		"count" : 1,
	},
  {
    "name": `"Map Device"-part`,
    "count" : 1,
  },
  {
    "name": `"Map Device"-part`,
    "count" : 1,
  },
	{
		"name": `"Stone Tablet" (Old & Worn)`,
		"count": 1,
	},
  {
    "name": `"Stone Tablet" (Relatively New)`,
    "count": 1,
  },
];

function loadInventoryItems() {
	const fragment = document.getElementById('exile-inventory-item').shadowRoot.getElementById('inventory-item-template');

	inventoryItems.forEach(c => {
		const instance = document.importNode(fragment.content, true);

		instance.querySelector('.name').innerHTML = c.name;
		instance.querySelector('.count').innerHTML = c.count;

		document.getElementById('items').appendChild(instance);
	})
}

window.onload = () => {
	loadInventoryItems();
};
