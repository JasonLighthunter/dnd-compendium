const LIFE_STATE = {
	ALIVE: "Alive",
	DEAD: "Deceased",
	UNKNOWN: "Unknown",
	UNCLEAR: "Unclear",
	KILLED: "Killed"
};

const characters = [
	{
		"name": "Dominus, High Templar of the Order of Innocence",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Enigmatic Leader of the Templar Order and responsible for your exile.
		`
	},
	{
		"name": "Hillock",
		"status": LIFE_STATE.KILLED,
		"description" : `
			The undead gatekeeper of Lioneye's Watch. Upon his dead was engulfed in green flames accompanied by a voice stating that he would be claimed by the 'Undead Syndicate'.
			The flames were extinguished by 3 helpings of holy water.
		`,
	},
	{
		"name": `"Unknown Female Voice"`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			Mysterious female sounding voice that wanted to claim Hillock's corpse for the Immortal Syndicate.
		`,
	},
	{
		"name": "Nessa",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Young woman who tried to keep the refugees at Lioneye's Watch fed and healthy.
		`,
	},
	{
		"name": "Marceus Lioneye",
		"status": LIFE_STATE.DEAD,
		"description" : `
			Commander of the legion posted at his fort, Lioneye's Watch, about a 100 years ago. You encountered his headless corpse on top of a pile in the basement of Lioneye's Watch.
		`,
	},
	{
		"name": "King Kaom",
		"status": LIFE_STATE.DEAD,
		"description" : `
			Legendary Orcish Warrior King who defeated Lioneye's legion and granted Marceus Lioneye "a place on his belt".
		`,
	},
	{
		"name": "Rake",
		"status": LIFE_STATE.KILLED,
		"description" : `
			Orc thug aspiring to be Kaoms successor.
			You encountered him at the small peninsula from where he led his small group of bandits.
		`,
	},
	{
		"name": "Valdo Brandyr",
		"status": LIFE_STATE.DEAD,
		"description" : `
			Late grandfather of Eloise Brandyr and played a part in the creation of D-3.
			Was an esteemed scolar at the Oriath Academy.
			Seemingly worked on some "Map Device" for the late High Templar Cavas Venarius.
		`,
	},
	{
		"name": "Eloise Brandyr",
		"status": LIFE_STATE.UNCLEAR,
		"description" : `
			Youngest member of the noble house of Brandyr and protégé of D-3.
			She Was exiled 5 years ago together with D-3.
			She was last seen in a vision of some sorts in the courtyard of Axiom Prison.
		`,
	},
	{
		"name": "Cavas Venarius",
		"status": LIFE_STATE.DEAD,
		"description" : `
			The High Templar before Dominus.
			He apparantly asked Valdo Brandyr to create a certain device and had it shipped to Wreaclast.
		`,
	},
	{
		"name": "Tarkleigh",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Young man who acts as head of the scouting and foraging party supplying the refugees.
			Urged you to find a "safe" route inland where it would be easier to survive.
		`,
	},
	{
		"name": "Tarkleigh",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Young man who acts as head of the scouting and foraging party supplying the refugees.
			Urged you to find a 'safe' route inland where it would be easier to survive.
		`,
	},
	{
		"name": "Kadavrus",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Necromancer that is kept company by his skeletal birds. Diciple of Shavrone of Umbra.
		`,
	},
	{
		"name": `Shavrone of Umbra "The Umbra"`,
		"status": LIFE_STATE.DEAD,
		"description" : `
			Overseer of Axiom Prison and original Creator of Brutus, the warden of the prison.
			Dominus ordered Piety to investigate her and her work.
		`,
	},
	{
		"name": `"Emperor" Chitus`,
		"status": LIFE_STATE.DEAD,
		"description" : `
			Answer to the riddle "Speak the name of the emperor!" at the end of the long tunnel behind the gold mask in Lioneye's Watch.
		`,
	},
	{
		"name": "Navali",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Peculiar "'seer" of unknown origin.
			She is able to divine the future using large silver coins.
		`,
	},
	{
		"name": "Alva Valai",
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			Self-proclaimed "Treasure Hunter Extraordinaire" who is Looking for the "Lost Temple of Atzoatl".
			During the first meeting with the exiles she seemed to have an interest in the "Map Device".
			Their second meeting was behind a string rift leading to a Temple she identified as "Atzoatl" in the past. Never seen after returing to the present.
		`,
	},
	{
		"name": "Atzix",
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			His name was mentioned by duergar encountered during a trip back in time in Axiom Prison.
		`,
	},
	{
		"name": "Vato-zel, Vaal Architect of the Armory",
		"status": LIFE_STATE.KILLED,
		"description" : `
			His name was mentioned by duergar encountered during a trip back in time in Axiom Prison.
		`,
	},
	{
		"name": `"Lady" Piety`,
		"status": LIFE_STATE.ALIVE,
		"description" : `
			The only arcane magic caster the Templar Order permits to act as they please.
			She has severe temper issues and seems very trigger happy.
			She apparantly loves kobolds for their skin.
			Fled during the fight with Brutus, the Warden.
		`,
	},
	{
		"name": `"Unknown Hooded Giant Tentacle-Bearded Man"`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `"Unknown Protector of Eloise Brandyr" (Templar, Male)`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `"Unknown Protector of Eloise Brandyr" (Sorceress, Female)`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `"Unknown Protector of Eloise Brandyr" (Swordfighter, Male)`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `"The Goddess"`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			Depicted with bronze statues of a blindfolded woman with scales in her hand.
			Seemingly the judge of the Bronze Trials.
		`,
	},
	{
		"name": "Brutus, Warden of Axiom Prison",
		"status": LIFE_STATE.KILLED,
		"description" : `
			While born as a elven male, countless experiments have rendered him a hulking monstrocity.
			Served his eternal task of guarding Axiom Prison until the exiles killed him after Piety orchestrated thier battle.
		`,
	},
	{
		"name": `"The Inquisitor"`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `"The Soulless One"`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": "Malachai",
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `Hagar, the "Kobold"`,
		"status": LIFE_STATE.KILLED,
		"description" : `
			He is a 'Kobold' much taller than other kobolds.
			Commands an army of earthen golems that, according to his claims, are other kobolds.
			Member of the Immortal Syndicate and anwers to "The big boss".
			Killed by the exiles after he intended to march out to Lioneye's Watch stating "I will conquer the coast and deal with anyone who stands in my way".
			Upon being crushed by a brutal and devastating trap laid by the exiles, his corpse burned up in green flames leaving nothing behind while his legion turned to sand.
		`,
	},
	{
		"name": `"The Big Boss"`,
		"status": LIFE_STATE.ALIVE,
		"description" : `
			???
		`,
	},
	{
		"name": `"Unknown Male Voice of Bronze Trials"`,
		"status": LIFE_STATE.UNKNOWN,
		"description" : `
			???
		`,
	},
	{
		"name": `"Singing Woman"`,
		"status": LIFE_STATE.ALIVE,
		"description" : `
			A bronzed woman with long flowing brown hair singing a song to the sea.
			The exiles have observed her luring a corpse to her and saw her feeding it to the creatures crawling underneath the sand.
		`,
	},
	{
		"name": `"Glowing man"`,
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Man spotted in a shipwreck at the coast littered with frost, shipwrecks and the singing woman.
			Seemed to glow with an odd unnatural light.
		`,
	},
	{
		"name": "Captain Arteri",
		"status": LIFE_STATE.KILLED,
		"description" : `
			Captain of a group of Blackguards tasked with defending the pass between the forest and the valley in front of Axiom Prison.
			He had a relationship with Piety and saw his men as living shields and did not doubt to use their lives as fuel for his gem magic.
		`,
	},
	{
		"name": "Tony",
		"status": LIFE_STATE.ALIVE,
		"description" : `
			Blackguard spared and conscripted as a sidekick by the exiles.
			He seems to be sick of Wreaclast and wants to go home to his wife and son. Seems to know the area quite well and is willing to guide the exiles as long as the journey leads to the ship at the bluff where the soldiers docked.
		`,
	}
];

function loadCharacters() {
	const fragment = document.getElementById('exile-character').shadowRoot.getElementById('character-template');

	characters.forEach(c => {
		const instance = document.importNode(fragment.content, true);

		instance.querySelector('.name').innerHTML = c.name;
		instance.querySelector('.status').innerHTML = `Status: ${c.status}`;
		instance.querySelector('.description').innerHTML = c.description;

		document.getElementById('characters').appendChild(instance);
	})
}

window.onload = () => {
	loadCharacters();
};
