const items = [
	{
		"name": `Silver Coin`,
		"description" : `
			A large silver coin the size of a grown persons palm.
			Can be given to a seer to have them prophesize the future.
		`
	},
	{
		"name": `"Gold Rimmed Gems"`,
		"description" : `
			Gems found only on the continent of Wreaclast.
			Can be used to cast spells of the highest level the user is capable of.
			They come in 3 variations:

			"Red"(Damage),
			"Blue"(Buff / Debuff),
			"Green" (Other)
		`
	},
	{
		"name": `"Map Device"`,
		"description" : `
			A strange device that looks like a mixture of a miniature planetarium and a sundial.
			On the front is a drawer with the sizes, 20 cm x 20 cm x 10 cm.
			It triggers strange visions depending on the item inserted.
			Inserting an item activates the device and destroys the item in the process.
		`,
	},
	{
		"name": `"Stone Tablet"`,
		"description" : `
			A slab of stone engraved with numerous runes, glyphs and other symbols.
			Its dimensions almost exactly match the "Map Device"'s drawer.
			Age, style and material seem to differ for each slab.
		`,
	},
	{
		"name": `"Red Rimmed Gems"`,
		"description" : `
			Appart from the color of the rims indisinguishable from its gold counterpart.
		`
	},
];

function loadItems() {
	const fragment = document.getElementById('exile-item').shadowRoot.getElementById('item-template');

	items.forEach(c => {
		const instance = document.importNode(fragment.content, true);

		instance.querySelector('.name').innerHTML = c.name;
		instance.querySelector('.description').innerHTML = c.description;

		document.getElementById('items').appendChild(instance);
	})
}

window.onload = () => {
	loadItems();
};
